#!/usr/bin/rdmd --shebang --main -unittest -g
import std.algorithm;
import std.array;
import std.conv;
import std.stdio;
import std.string;
int token;
string id;
string input;
Op[] prec;
int[string] tokenMap;
enum
{
    TOKEN_EOF = 0,
    TOKEN_LAST_CHAR = 127,
    TOKEN_INT,
    TOKEN_OTHER
}
bool nextToken()
{
    id = "";
    input = input
        .stripLeft;
    if (input.empty)
    {
        token = TOKEN_EOF;
        return false;
    }
    if (input[0] == '0')
    {
        if (!id.empty)
            return false;
        if (input.length > 1 && input[1] >= '0' && input[1] <= '9')
            return false;
        id ~= input[0];
        input = input[1..$];
        token = TOKEN_INT;
    }
    else if (input[0] >= '0' && input[0] <= '9')
    {
        while(!input.empty && input[0] >= '0' && input[0] <= '9')
        {
            id ~= input[0];
            input = input[1..$];
        }
        token = TOKEN_INT;
    }
    else if (input[0] == '(' || input[0] == ')')
    {
        id ~= input[0];
        token = input[0];
        input = input[1..$];
    }
    else
    {
        auto candidates = prec
            .joiner
            .array
            .sort
            .uniq
            .filter!(a => a.startsWith(input[0]) || a == input[0].to!string)
            .array;
        string bookmark = input.dup;
        string bestMatch;
        string bestMatchBookmark;
        id ~= input[0];
        input = input[1..$];
        while (candidates.any!(a => a.startsWith(id) || a == id))
        {
            auto matchResult = candidates.find!(a => a == id);
            if (!matchResult.empty)
            {
                bestMatch = matchResult.array[0];
                bestMatchBookmark = input.dup;
            }
            if (input.empty)
                break;
            id ~= input[0];
            input = input[1..$];
        }
        if (!bestMatch.empty)
        {
            id = bestMatch;
            input = bestMatchBookmark;
            token = id.length == 1
                ?  id[0]
                : tokenMap[id];
        }
        else
        {
            input = bookmark;
            id ~= input[0];
            input = input[1..$];
            token = TOKEN_OTHER;
        }
    }
    return true;
}
void initStream(string s)
{
    input = s;
    nextToken();
}
class Node
{
    string op;
    enum { BINARY, UNARY }
    int type;
    Expression lhs;
    Expression rhs;
    override string toString()
    {
        return type == BINARY
        ? format("(%s %s %s)", op, lhs, rhs)
        : format("(%s %s)", op, rhs);
    }
}
class Expression
{
    enum { NODE, ID }
    int type;
    union { Node node; string id; }
    this (int t, string i) { type = t; id = i; }
    this (int t, Node n) { type = t; node = n; }
    this(string op, int op_type, Expression lhs, Expression rhs)
    {
        node = new Node;
        node.op = op;
        if (op_type == Node.BINARY)
            node.lhs = lhs.dup;
        else
        {
            node.type = Node.UNARY;
            node.lhs = null;
        }
        node.rhs = rhs;
        type = NODE;
    }
    Expression dup()
    {
        return type == NODE
            ? new Expression(type, node)
            : new Expression(type, id);
    }
    override string toString() { return type == NODE ? node.toString() : id; }
}
struct Op
{
    enum { LEFT, RIGHT }
    string[] tokens;
    alias tokens this;
    int associativity;
    int type;
    this (string[] t, int a, int type)
    {
        static _id = TOKEN_OTHER;
        foreach (tid; t.filter!(a => a.length > 1))
        {
            assert(tid !in tokenMap);
            tokenMap[tid] = ++_id;
        }
        tokens = t;
        associativity = a;
        this.type = type;
    }
    bool opBinaryRight(string op)(string lhs)
        if (op == "in")
    { return tokens.canFind(lhs); }
}
Expression parseFactor()
{
    if (token == TOKEN_INT)
    {
        auto val = new Expression(Expression.ID, id);
        nextToken();
        return val;
    }
    else if (token == '(')
    {
        nextToken();
        auto val = parseExpression();
        assert(token == ')');
        nextToken();
        return val;
    }
    assert(false);
}
Expression parse(int n)
{
    Expression val;
    if (n >= prec.length)
        val = parseFactor();
    else
    {
        if (prec[n].type == Node.BINARY)
            val = parse(n+1);
        if (prec[n].associativity == Op.LEFT)
        {
            while (id in prec[n])
            {
                auto op = id;
                nextToken();
                val = new Expression(op, prec[n].type, val, parse(n+1));
            }
        }
        else if (id in prec[n])
        {
            auto op = id;
            nextToken();
            val = new Expression(op, prec[n].type, val, parse(n+1));
        }
        else if (prec[n].type == Node.UNARY)
            val = parse(n+1);
    }
    return val;
}
Expression parseExpression() { return parse(0); };
void assertToken(int x) { assert (token == x, id); nextToken(); }
void assertTokenInt(string x) { assert (token == TOKEN_INT && id == x, id); nextToken(); }
unittest
{
    prec.length = 0;
    tokenMap.clear;
    prec ~= Op(["+", "-", "|", "^"            ], Op.LEFT , Node.BINARY);
    prec ~= Op(["*", "/", "%", "&", "<<", ">>"], Op.LEFT , Node.BINARY);
    prec ~= Op(["-", "~"                      ], Op.RIGHT, Node.UNARY );
    prec ~= Op(["^^"                          ], Op.RIGHT, Node.BINARY);
    initStream("0 12345 10");
    assertTokenInt("0");
    assertTokenInt("12345");
    assertTokenInt("10");
    assertToken(TOKEN_EOF);
    initStream("-+|^*/%&~()<<>>^^");
    assertToken('-');
    assertToken('+');
    assertToken('|');
    assertToken('^');
    assertToken('*');
    assertToken('/');
    assertToken('%');
    assertToken('&');
    assertToken('~');
    assertToken('(');
    assertToken(')');
    assertToken(tokenMap["<<"]);
    assertToken(tokenMap[">>"]);
    assertToken(tokenMap["^^"]);
    assertToken(TOKEN_EOF);
}
unittest
{
    prec.length = 0;
    tokenMap.clear;
    prec ~= Op(["+", "-", "|", "^"            ], Op.LEFT , Node.BINARY);
    prec ~= Op(["*", "/", "%", "&", "<<", ">>"], Op.LEFT , Node.BINARY);
    prec ~= Op(["-", "~"                      ], Op.RIGHT, Node.UNARY );
    prec ~= Op(["^^"                          ], Op.RIGHT, Node.BINARY);
    initStream("12*(34 + 45)/56 + ~25^^42");
    parseExpression()
        .writeln();
}
