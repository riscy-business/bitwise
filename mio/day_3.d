#!/usr/bin/rdmd --shebang --main -unittest
import std.conv;
import std.stdio;
import std.string;
int token;
string id;
string input;
enum
{
    TOKEN_EOF = 0,
    TOKEN_LAST_CHAR = 127,
    TOKEN_INT,
    TOKEN_LSHIFT,
    TOKEN_RSHIFT,
    TOKEN_OTHER
}
bool nextToken()
{
    id = "";
    input = input
        .stripLeft;
    if (input.empty)
    {
        token = TOKEN_EOF;
        return false;
    }
    switch (input[0])
    {
        case '0':
            if (!id.empty)
                return false;
            if (input.length > 1 && input[1] >= '0' && input[1] <= '9')
                return false;
            id ~= input[0];
            input = input[1..$];
            token = TOKEN_INT;
            break;
        case '1': .. case '9':
            while(!input.empty && input[0] >= '0' && input[0] <= '9')
            {
                id ~= input[0];
                input = input[1..$];
            }
            token = TOKEN_INT;
            break;
        case '-', '+', '|', '^', '*', '/', '%', '&', '~':
            id ~= input[0];
            token = input[0];
            input = input[1..$];
            break;
        case '<':
            id ~= input[0];
            input = input[1..$];
            if (input[0] != '<')
                return false;
            id ~= input[0];
            input = input[1..$];
            token = TOKEN_LSHIFT;
            break;
        case '>':
            id ~= input[0];
            input = input[1..$];
            if (input[0] != '>')
                return false;
            id ~= input[0];
            input = input[1..$];
            token = TOKEN_RSHIFT;
            break;
        default:
            id ~= input[0];
            input = input[1..$];
            token = TOKEN_OTHER;
            break;
    }
    return true;
}
void initStream(string s)
{
    input = s;
    nextToken();
}
class Node
{
    string op;
    enum { BINARY, UNARY }
    int type;
    Expression lhs;
    Expression rhs;
    override string toString()
    {
        return type == BINARY
        ? format("(%s %s %s)", op, lhs, rhs)
        : format("(%s %s)", op, rhs);
    }
}
class Expression
{
    enum { NODE, ID }
    int type;
    union { Node node; string id; }
    this (string i) { type = ID; id = i; }
    this (Node n) { type = NODE; node = n; }
    this(string op, Expression rhs)
    {
        node = new Node;
        node.op = op;
        node.type = Node.UNARY;
        node.lhs = null;
        node.rhs = rhs;
        type = NODE;
    }
    this(string op, Expression lhs, Expression rhs)
    {
        node = new Node;
        node.op = op;
        node.lhs = lhs.dup;
        node.rhs = rhs;
        type = NODE;
    }
    Expression dup()
    {
        return type == NODE
            ? new Expression(node)
            : new Expression(id);
    }
    override string toString() { return type == NODE ? node.toString() : id; }
}
Expression parseFactor()
{
    if (token == TOKEN_INT)
    {
        auto val = new Expression(id);
        nextToken();
        return val;
    }
    assert(false);
}
Expression parsePrecedence2()
{
    Expression val;
    if (token == '-' || token == '~')
    {
        auto op = id;
        nextToken();
        val = new Expression(op, parseFactor());
    }
    else
        val = parseFactor();
    return val;
}
Expression parsePrecedence1()
{
    auto val = parsePrecedence2();
    while (token == '*'
            || token == '/'
            || token == '%'
            || token == '&'
            || token == TOKEN_LSHIFT
            || token == TOKEN_RSHIFT)
    {
        auto op = id;
        nextToken();
        val = new Expression(op, val, parsePrecedence2());
    }
    return val;
}
Expression parsePrecedence0()
{
    auto val = parsePrecedence1();
    while (token == '+'
            || token == '-'
            || token == '|'
            || token == '^')
    {
        auto op = id;
        nextToken();
        val = new Expression(op, val, parsePrecedence1());
    }
    return val;
}
alias parseExpression = parsePrecedence0;
long evalFactor()
{
    if (token == TOKEN_INT)
    {
        auto val = id.to!long;
        nextToken();
        return val;
    }
    assert(false);
}
long evalPrecedence2()
{
    long val;
    if (token == '-' || token == '~')
    {
        auto op = token;
        nextToken();
        if (op == '-')
            val = -evalFactor();
        else if (op == '~')
            val = ~evalFactor();
    }
    else
        val = evalFactor();
    return val;
}
long evalPrecedence1()
{
    auto val = evalPrecedence2();
    while (token == '*'
            || token == '/'
            || token == '%'
            || token == '&'
            || token == TOKEN_LSHIFT
            || token == TOKEN_RSHIFT)
    {
        auto op = token;
        nextToken();
        /* val = new Expression(op, val, evalPrecedence2()); */
        switch (op)
        {
            case '*':
                val *= evalPrecedence2();
                break;
            case '/':
                val /= evalPrecedence2();
                break;
            case '%':
                val %= evalPrecedence2();
                break;
            case '&':
                val &= evalPrecedence2();
                break;
            case TOKEN_LSHIFT:
                val <<= evalPrecedence2();
                break;
            case TOKEN_RSHIFT:
                val >>= evalPrecedence2();
                break;
            default:
                assert(false);
                break;
        }
    }
    return val;
}
long evalPrecedence0()
{
    auto val = evalPrecedence1();
    while (token == '+'
            || token == '-'
            || token == '|'
            || token == '^')
    {
        auto op = token;
        nextToken();
        /* val = new Expression(op, val, evalPrecedence1()); */
        switch (op)
        {
            case '+':
                val += evalPrecedence1();
                break;
            case '-':
                val -= evalPrecedence1();
                break;
            case '|':
                val |= evalPrecedence1();
                break;
            case '^':
                val ^= evalPrecedence1();
                break;
            default:
                assert(false);
                break;
        }
    }
    return val;
}
alias evalExpression = evalPrecedence0;
enum vmOp
{
    LIT,
    SUB,
    PLUS,
    OR,
    XOR,
    MUL,
    DIV,
    MOD,
    AND,
    LSHIFT,
    RSHIFT,
    NEG,
    NOT,
    HALT
}
int[] compile(Expression ast)
{
    assert(ast.type == Expression.NODE);
    Node[] prevNodes;
    prevNodes ~= ast.node;
    ast = ast.node.lhs;
    while(!prevNodes.empty)
    {
        if (ast.type == Expression.ID)
        {
            writeln(ast.id);
            if (ast == prevNodes[$-1].rhs)
            {
                writeln(prevNodes[$-1].op);
                prevNodes.length -= 1;
            }
            ast = prevNodes[$-1].rhs;
        }
        else
        {
            if (ast == prevNodes[$-1].rhs)
            {
                writeln(prevNodes[$-1].op);
                prevNodes.length -= 1;
                ast = prevNodes[$-1].rhs;
            }
            else
            {
                prevNodes ~= ast.node;
                ast = ast.node.lhs;
            }
        }
    }
    writeln("halt");
    int[] result;
    return result;
}
int vmExec(int[] code)
{
    int[] stack;
    for(;;)
    {
        int op = code[0];
        code = code[1..$];
        switch (op) with (vmOp)
        {
            case LIT:
                stack ~= code[0];
                code = code[1..$];
                break;
            case SUB:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs - rhs;
                break;
            case PLUS:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs + rhs;
                break;
            case OR:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs | rhs;
                break;
            case XOR:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs ^ rhs;
                break;
            case MUL:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs * rhs;
                break;
            case DIV:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs / rhs;
                break;
            case MOD:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs % rhs;
                break;
            case AND:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs & rhs;
                break;
            case LSHIFT:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs << rhs;
                break;
            case RSHIFT:
                auto rhs = stack[$-1];
                auto lhs = stack[$-2];
                stack.length -= 2;
                stack ~= lhs >> rhs;
                break;
            case NEG:
                auto rhs = stack[$-1];
                stack[$-1] = -rhs;
                break;
            case NOT:
                auto rhs = stack[$-1];
                stack[$-1] = ~rhs;
                break;
            case HALT:
                return stack[$-1];
            default:
                assert(false);
                break;
        }
    }
}
unittest
{
    initStream("12*34 + 45/56 + ~25");
    evalExpression()
        .writeln();
}
unittest
{
    initStream("12*34 + 45/56 + ~25");
    parseExpression()
        .writeln();
}
unittest
{
    int[] code;
    code ~= vmOp.LIT; code ~= 12;
    code ~= vmOp.LIT; code ~= 34;
    code ~= vmOp.MUL;
    code ~= vmOp.LIT; code ~= 45;
    code ~= vmOp.LIT; code ~= 56;
    code ~= vmOp.DIV;
    code ~= vmOp.PLUS;
    code ~= vmOp.LIT; code ~= 25;
    code ~= vmOp.NOT;
    code ~= vmOp.PLUS;
    code ~= vmOp.HALT;
    vmExec(code)
        .writeln();
}
unittest
{
    initStream("12*34 + 45/56 + ~25");
    parseExpression()
        .compile()
        .vmExec()
        .writeln();
}
